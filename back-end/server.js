var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json())
const env = require('./src/config/env.js');
 
require('./src/router/router.js')(app);

const db = require('./src/config/db.config.js');

const Role = db.role;
  
// force: true will drop the table if it already exists
/*db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
  initial();
});*/
 
//require('./app/route/project.route.js')(app);
 
// Create a Server
var server = app.listen(8080, function () {
 
  var host = env.host
  var port = server.address().port

  console.log('HOST; '+host);
 
  console.log("App listening at http://%s:%s", host, port)
})


function initial(){
	Role.create({
		id: 1,
		name: "USER"
	});
	
	Role.create({
		id: 2,
		name: "ADMIN"
	});
	
	Role.create({
		id: 3,
		name: "PM"
	});
}