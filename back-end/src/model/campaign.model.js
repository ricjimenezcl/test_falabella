module.exports = (sequelize, Sequelize) => {
	const Campaign = sequelize.define('campaign', {
	  name: {
		  type: Sequelize.STRING
	  },
	  code: {
		  type: Sequelize.STRING
	  },
	  start_date: {
		  type: Sequelize.STRING
	  },
	  end_date: {
		  type: Sequelize.STRING
	  }
	});
	
	return Campaign;
}