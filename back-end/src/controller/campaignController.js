const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Campaign = db.campaign;


exports.createCampaign = async (req, res) => {
    const campaignModel =await Campaign.build({
      name: req.body.name,
      code: req.body.code,
      start_date: req.body.start_date,
      end_date: req.body.end_date
 });
 await campaignModel.save()
 if(!campaignModel){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
 });
 }
 res.status(200).send({
    status: 200,
    message: 'Data Save Successfully'
 });
 }

 exports.updateCampaign = async (req, res) => {
   //const id=req.body.id;
   const campaignModel =await Campaign.update({
      name: req.body.name,
      code: req.body.code,
      start_date: req.body.start_date,
      end_date: req.body.end_date
},
{where: {code: req.body.code} });
if(!campaignModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
res.status(200).send({
   status: 200,
   message: 'Data Update Successfully'
});
}

exports.deleteCampaign = async (req, res) => {
   try {
   //const id= req.body.id;
   const campaignModel = await Campaign.destroy({
   where: { code: req.body.code }
});
if(!campaignModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
res.status(200).send({
   status: 200,
   message: 'Data Delete Successfully'
});
   }catch (err) {
      console.error(`Error reset tabla General`);
    }
}

exports.getCampaignById = async (req, res) => {
   try {
   //const id= req.body.id;
   const campaignModel = await Campaign.findOne({
   where: { code: req.body.code }
});
if(!campaignModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
return res.json(campaignModel);
   }catch (err) {
      console.error(`Error reset tabla General`);
    }
}

exports.getCampaign = async (req, res) => {
   try {
   //const id= req.body.id;
   const campaignModel = await Campaign.findAll();
if(!campaignModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
return res.json(campaignModel);
   }catch (err) {
      console.error(`Error reset tabla General`);
    }
}


