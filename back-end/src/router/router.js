const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');

module.exports = function(app) {

    const controller = require('../controller/controller.js');
	const controllercampaign = require('../controller/campaignController.js');
 
	app.post('/api/auth/signup', [verifySignUp.checkDuplicateUserNameOrEmail, verifySignUp.checkRolesExisted], controller.signup);
	
	app.post('/api/auth/signin', controller.signin);

	app.post('/api/newcampaign', controllercampaign.createCampaign);

	app.post('/api/updatecampaign', controllercampaign.updateCampaign);

	app.post('/api/deletecampaign', controllercampaign.deleteCampaign);

	app.get('/api/getcampaign', controllercampaign.getCampaign);

	app.get('/api/getCampaignbyid', controllercampaign.getCampaignById);

	app.get('/api/test/user', [authJwt.verifyToken], controller.userContent);
	
	app.get('/api/test/pm', [authJwt.verifyToken, authJwt.isPmOrAdmin], controller.managementBoard);
	
	app.get('/api/test/admin', [authJwt.verifyToken, authJwt.isAdmin], controller.adminBoard);
}